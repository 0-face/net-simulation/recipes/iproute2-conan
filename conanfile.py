from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

class Recipe(ConanFile):
    name           = "iproute2"
    description    = "Iproute2 is a collection of utilities for controlling" \
                   + " TCP / IP networking and traffic control in Linux."
    license        = "GPLv2"

    version        = "4.4.0"
    settings       = "os", "compiler", "arch"

    homepage       = "https://wiki.linuxfoundation.org/networking/iproute2"
    source_sha256  = "a24e129669155de55c55687b00bfe78a56215a5b54c4ca6372dcbbbcbd7b11eb"

    url            = "https://gitlab.com/0-face/conan-recipes/iproute2-conan"

    options = {
        'extra_cflags': "ANY",
        'extra_ldflags': "ANY"
    }

    default_options = (
        'extra_cflags=',
        'extra_ldflags='
    )

    def configure(self):
        # package is pure c
        del self.settings.compiler.libcxx

    def source(self):
        zip_name = self.name + ".tar.gz"
        url = "https://mirrors.edge.kernel.org/pub/linux/utils/net/iproute2/iproute2-{}.tar.gz" \
                .format(self.version)

        zip_path = path.join(self.source_folder, zip_name)

        if path.exists(zip_path) and tools.sha256sum(zip_path) == self.source_sha256:
            self.output.info("Skipped source download ('%s' found)" % zip_name)
        else:
            self.output.info("Downloading source code ...")
            tools.download(url, zip_name)
            tools.check_sha256(zip_name, self.source_sha256)

        self.output.info("Extracting zip...")
        tools.unzip(zip_name)

    def build(self):
        self.patch_sources()

        self.try_make_dir(self.install_dir)

        with tools.chdir(self.extracted_folder), \
            tools.environment_append(self.env_flags()):

            autotools = AutoToolsBuildEnvironment(self)
            autotools.make()
            autotools.install()

    def package(self):
        self.copy("*", src=self.install_dir_name, symlinks=True)

        self.copy("COPYING", src=self.extracted_folder_name)

    def package_info(self):
        self.cpp_info.bindirs = ['sbin']

        self.env_info.PATH.append(path.join(self.package_folder, "sbin"))

################################# Helpers ########################################################

    def patch_sources(self, strict=True):
        tc_core_c = path.join(self.extracted_folder, 'tc', 'tc_core.c')

        # missing stdint.h (required for UINT16_MAX symbol)
        tools.replace_in_file(
            tc_core_c,
            "#include <linux/atm.h>",
            '\n'.join([
                "#include <linux/atm.h>",
                "#include <stdint.h>"
            ]),
            strict=strict
        )

    def env_flags(self):
        env = {}

        env['DESTDIR'] = self.install_dir

        if self.options.extra_cflags:
            env['CFLAGS'] = str(self.options.extra_cflags)

        if self.options.extra_ldflags:
            env['LDFLAGS'] = str(self.options.extra_ldflags)

        return env

    def run_and_log(self, cmd, cwd=None, env=None):
        msg = ''
        if cwd:
            msg = "cwd='{}'\n".format(cwd)
        if env:
            msg += "env='{}'\n".format(env)

        if cwd or env:
            msg += "\t"

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

    def try_make_dir(self, directory):
        import errno

        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

######################################## Properties ####################################################

    @property
    def install_dir_name(self):
        return 'install'

    @property
    def install_dir(self):
        return path.join(self.build_folder, self.install_dir_name)

    @property
    def extracted_folder_name(self):
        return '{}-{}'.format(self.name, self.version)

    @property
    def extracted_folder(self):
        return path.join(self.source_folder, self.extracted_folder_name)
